package net.acsid.brpg.compagnion;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Ui extends javax.swing.JFrame {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5544584545919600540L;
	public Ui() {
		initComponents();
	}
	
	JScrollPane jScrollPane1;
	JTextArea jTextArea1;
	private void initComponents() {
		jScrollPane1 = new javax.swing.JScrollPane();
		jTextArea1 = new javax.swing.JTextArea();
		
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		
		jTextArea1.setColumns(20);
		jTextArea1.setRows(5);
		jScrollPane1.setViewportView(jTextArea1);
		
		
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(jScrollPane1,javax.swing.GroupLayout.PREFERRED_SIZE,406, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(182,Short.MAX_VALUE))
		);
		
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(jScrollPane1,javax.swing.GroupLayout.PREFERRED_SIZE,243,javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(23,Short.MAX_VALUE))
		);
		
		setTitle("BattleRPG - Compagion");
		
		pack();
	}

}
