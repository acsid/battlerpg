package net.acsid.brpg.Engine;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

import net.acsid.brpg.compagnion.Ui;

public class Renderer {

	
	public Renderer() {
		//load properties later here
		
		
	}
	
	Ui ui = null;
	
	
	public void start(Ui ui) {
		this.ui = ui;
		
		
		
		//initialise opengl
		
		try {
			Display.setDisplayMode(new DisplayMode(800,600));
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, 800, 0, 600, 1, -1);
		
		
		while ( !Display.isCloseRequested() ) {
			
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			
			//place Holder for graphics
			
			
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glVertex2f(100, 100);
				GL11.glVertex2f(100+200, 100);
				GL11.glVertex2f(100+200, 100+200);
				GL11.glVertex2f(100, 100+200);
			GL11.glEnd();
				
			
			
			
			Display.update();
		}
		
		Display.destroy();
	}
	
	
	
	
}
